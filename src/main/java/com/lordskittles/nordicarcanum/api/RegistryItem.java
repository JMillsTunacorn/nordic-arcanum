package com.lordskittles.nordicarcanum.api;

public class RegistryItem<T>
{
	private String mKey;
	private T mValue;
	
	public RegistryItem(String aKey, T aValue)
	{
		mKey = aKey;
		mValue = aValue;
	}
	
	public String Key()
	{
		return mKey;
	}
	
	public T Value()
	{
		return mValue;
	}
}
