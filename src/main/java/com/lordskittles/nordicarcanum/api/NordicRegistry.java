package com.lordskittles.nordicarcanum.api;

import java.util.ArrayList;

import com.lordskittles.nordicarcanum.blocks.IItemGroupHolder;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.world.gen.feature.Feature;
import net.minecraftforge.event.RegistryEvent;

public class NordicRegistry 
{
	private static ArrayList<RegistryItem<Block>> mBlockRegistry = new ArrayList<RegistryItem<Block>>();
	private static ArrayList<RegistryItem<Item>> mItemRegistry = new ArrayList<RegistryItem<Item>>();
	private static ArrayList<RegistryItem<Feature<?>>> mFeatureRegistry = new ArrayList<RegistryItem<Feature<?>>>();
	
	public static void RegisterItems(final RegistryEvent.Register<Item> aEvent)
	{
		for(RegistryItem<Item> item : mItemRegistry)
		{
			aEvent.getRegistry().register(item.Value());
		}
		
		for(RegistryItem<Block> block : mBlockRegistry)
		{
			aEvent.getRegistry().register(new BlockItem(block.Value(), new Item.Properties().group(((IItemGroupHolder)block.Value()).Group())).setRegistryName(block.Key()));
		}
	}
	
	public static void RegisterBlocks(final RegistryEvent.Register<Block> aEvent)
	{
		for(RegistryItem<Block> block : mBlockRegistry)
		{
			aEvent.getRegistry().register(block.Value());
		}
	}
	
	public static void RegisterFeatures(final RegistryEvent.Register<Feature<?>> aEvent)
	{
		for(RegistryItem<Feature<?>> feature : mFeatureRegistry)
		{
			aEvent.getRegistry().register(feature.Value());
		}
	}
	
	public static void QueueItemForRegister(Item aItem)
	{
		mItemRegistry.add(new RegistryItem<Item>(aItem.getRegistryName().toString(), aItem));
	}
	
	public static void QueueBlockForRegister(Block aBlock)
	{
		mBlockRegistry.add(new RegistryItem<Block>(aBlock.getRegistryName().toString(), aBlock));
	}
	
	public static void QueueFeatureForRegister(Feature<?> aFeature) 
	{
		mFeatureRegistry.add(new RegistryItem<Feature<?>>(aFeature.getRegistryName().toString(), aFeature));
	}
}
