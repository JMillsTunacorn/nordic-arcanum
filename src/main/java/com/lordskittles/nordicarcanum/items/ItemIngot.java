package com.lordskittles.nordicarcanum.items;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicItemGroup;

public class ItemIngot extends ItemModBase
{
	public ItemIngot(String aIngotName)
	{		
		super(aIngotName + "_ingot", NordicItemGroup.Instance);
	}
}
