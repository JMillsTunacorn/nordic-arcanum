package com.lordskittles.nordicarcanum.items;

import com.lordskittles.nordicarcanum.api.NordicRegistry;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

public class ItemModBase extends Item
{
	protected String mRegistryName = "";
	
	public ItemModBase(String aNordicName, ItemGroup aGroup)
	{		
		super(new Item.Properties().group(aGroup));
		
		mRegistryName = aNordicName;
		setRegistryName(mRegistryName);
		
		NordicRegistry.QueueItemForRegister(this);
	}
	
	public String GetNordicRegistryName()
	{
		return mRegistryName;
	}
}
