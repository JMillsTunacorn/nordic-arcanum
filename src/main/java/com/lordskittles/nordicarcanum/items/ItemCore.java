package com.lordskittles.nordicarcanum.items;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicItemGroup;

public class ItemCore extends ItemModBase
{
	public ItemCore(String aCoreName)
	{
		super(aCoreName + "_core", NordicItemGroup.Instance);
	}
}
