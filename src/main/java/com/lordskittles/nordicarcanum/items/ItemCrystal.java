package com.lordskittles.nordicarcanum.items;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicWorldItemGroup;

public class ItemCrystal extends ItemModBase
{
	public ItemCrystal(String aCrystalName)
	{
		super(aCrystalName + "_crystal", NordicWorldItemGroup.Instance);
	}
}
