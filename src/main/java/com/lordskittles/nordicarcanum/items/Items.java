package com.lordskittles.nordicarcanum.items;

import com.lordskittles.nordicarcanum.NordicArcanum;
import com.lordskittles.nordicarcanum.api.NordicRegistry;

import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.ObjectHolder;

@Mod.EventBusSubscriber(modid = NordicArcanum.MODID, bus = Bus.MOD)
@ObjectHolder(NordicArcanum.MODID)
public class Items
{
	public static final Item arcane_dust = new ItemDust("arcane");
	public static final Item carnelian_dust = new ItemDust("carnelian");
	public static final Item calcite_dust = new ItemDust("calcite");
	public static final Item garnet_dust = new ItemDust("garnet");
	public static final Item amethyst_dust = new ItemDust("amethyst");

	public static final Item carnelian_crystal = new ItemCrystal("carnelian");
	public static final Item calcite_crystal = new ItemCrystal("calcite");
	public static final Item garnet_crystal = new ItemCrystal("garnet");
	public static final Item amethyst_crystal = new ItemCrystal("amethyst");
	
	public static final Item silver_ingot = new ItemIngot("silver");
	public static final Item nickle_ingot = new ItemIngot("nickle");
	public static final Item lead_ingot = new ItemIngot("lead");
	public static final Item copper_ingot = new ItemIngot("copper");
	public static final Item steel_ingot = new ItemIngot("steel");
	public static final Item black_metal_ingot = new ItemIngot("black_metal");
	
	public static final Item pheonix_feather_core = new ItemCore("pheonix_feather");
	public static final Item unicorn_hair_core = new ItemCore("unicorn_hair");
	public static final Item dragon_scale_core = new ItemCore("dragon_scale");
	public static final Item bat_wing_core = new ItemCore("bat_wing");
	public static final Item spider_silk_core = new ItemCore("spider_silk");

	@SubscribeEvent
	public static void Register(final RegistryEvent.Register<Item> aEvent)
	{
		NordicRegistry.RegisterItems(aEvent);
	}
}
