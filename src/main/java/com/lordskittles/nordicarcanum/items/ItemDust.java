package com.lordskittles.nordicarcanum.items;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicItemGroup;

public class ItemDust extends ItemModBase
{
	public ItemDust(String aDustName)
	{		
		super(aDustName + "_dust", NordicItemGroup.Instance);
	}
}
