package com.lordskittles.nordicarcanum.blocks;

import net.minecraft.item.ItemGroup;

public interface IItemGroupHolder
{
	ItemGroup Group();
	void SetGroup(ItemGroup aGroup);
}
