package com.lordskittles.nordicarcanum.blocks;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicWorldItemGroup;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;

public class BlockOre extends BlockModBase
{
	public BlockOre(String aOreName, int aHarvestLevel)
	{
		this(aOreName, aHarvestLevel, 0);
	}
	
	public BlockOre(String aOreName, int aHarvestLevel, int aLightLevel)
	{
		this(aOreName, aHarvestLevel, aLightLevel, 3.0F);
	}
	
	public BlockOre(String aOreName, int aHarvestLevel, int aLightLevel, float aHardness)
	{
		super(aOreName + "_ore", Material.ROCK, MaterialColor.RED, aHardness, 3.0F, SoundType.STONE, aLightLevel, aHarvestLevel);
		
		SetGroup(NordicWorldItemGroup.Instance);
	}
}
