package com.lordskittles.nordicarcanum.blocks;

import java.util.Random;
import java.util.function.Supplier;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicWorldItemGroup;
import com.lordskittles.nordicarcanum.api.NordicRegistry;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.BushBlock;
import net.minecraft.block.IGrowable;
import net.minecraft.block.trees.Tree;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemGroup;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.ForgeEventFactory;

public class BlockSapling extends BushBlock implements IGrowable, IItemGroupHolder
{
	public static final IntegerProperty Stage = BlockStateProperties.STAGE_0_1;
	protected static final VoxelShape mShape = Block.makeCuboidShape(2.0D,  0.0D, 2.0D, 14.0D, 12.0D, 14.0D);
	
	private final Supplier<Tree> mTree;
	
	public BlockSapling(Supplier<Tree> aTree, String aName)
	{
		super(Block.Properties.from(Blocks.DEAD_BUSH));
		
		mTree = aTree;
		
		setRegistryName(aName + "_sapling");
		NordicRegistry.QueueBlockForRegister(this);
	}
	
	@Override
	public VoxelShape getShape(BlockState aState, IBlockReader aWorld, BlockPos aPos, ISelectionContext aContext)
	{
		return mShape;
	}
	
	@Override
	public boolean isReplaceable(BlockState state, BlockItemUseContext useContext) 
	{
		return false;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void tick(BlockState aState, ServerWorld aWorld, BlockPos aPos, Random aRandom)
	{
		super.tick(aState, aWorld, aPos, aRandom); 
		if (!aWorld.isAreaLoaded(aPos, 1))
		{
			return;
		}
		
		if (aWorld.getLight(aPos.up()) >= 9 && aRandom.nextInt(7) == 0)
		{
			this.grow(aWorld, aPos, aState, aRandom);
		}
	}
	
	public void grow(ServerWorld aWorld, BlockPos aPos, BlockState aState, Random aRand)
	{
		if (aState.get(Stage) == 0)
		{
			aWorld.setBlockState(aPos, aState.cycle(Stage), 4);
		}
		else
		{
			if (!ForgeEventFactory.saplingGrowTree(aWorld, aRand, aPos) || !aWorld.canBlockSeeSky(aPos))
			{
				return;
			}
			
			this.mTree.get().func_225545_a_(aWorld, aWorld.getChunkProvider().getChunkGenerator(), aPos, aState, aRand);
		}
	}
	
	@Override
	public void grow(ServerWorld aWorld, Random aRand, BlockPos aPos, BlockState aState)
	{
		this.grow(aWorld, aPos, aState, aRand);
	}
	
	@Override
	public boolean canGrow(IBlockReader aReader, BlockPos aPos, BlockState aState, boolean aIsClient)
	{
		return true;
	}
	
	@Override
	public boolean canUseBonemeal(World aWorld, Random aRandom, BlockPos aPos, BlockState aState)
	{
		return (double)aWorld.rand.nextFloat() < 0.45D;
	}
	
	@Override
	public void fillStateContainer(Builder<Block, BlockState> aBuilder)
	{
		aBuilder.add(Stage);
	}

	@Override
	public ItemGroup Group()
	{
		return NordicWorldItemGroup.Instance;
	}

	@Override
	public void SetGroup(ItemGroup aGroup)
	{
		// TODO Auto-generated method stub
		
	}
}
