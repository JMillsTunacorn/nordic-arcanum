package com.lordskittles.nordicarcanum.blocks;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicWorldItemGroup;
import com.lordskittles.nordicarcanum.api.NordicRegistry;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.LeavesBlock;
import net.minecraft.item.ItemGroup;

public class BlockLeaves extends LeavesBlock implements IItemGroupHolder
{
	public BlockLeaves(String aName)
	{
		super(Block.Properties.from(Blocks.OAK_LEAVES)); 
		
		setRegistryName(aName + "_leaves");
		NordicRegistry.QueueBlockForRegister(this); 
	}

	@Override
	public ItemGroup Group()
	{
		return NordicWorldItemGroup.Instance;
	}

	@Override
	public void SetGroup(ItemGroup aGroup)
	{
		// TODO Auto-generated method stub
		
	}
}
