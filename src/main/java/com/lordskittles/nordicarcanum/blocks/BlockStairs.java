package com.lordskittles.nordicarcanum.blocks;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicWorldItemGroup;
import com.lordskittles.nordicarcanum.api.NordicRegistry;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.StairsBlock;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemTier;
import net.minecraftforge.common.ToolType;

public class BlockStairs extends StairsBlock implements IItemGroupHolder
{
	public BlockStairs(Block aBlock, String aName)
	{
		this(aBlock, aName, Material.ROCK, SoundType.STONE);
	}
	
	public BlockStairs(Block aBlock, String aName, Material aMaterial, SoundType aSound)
	{
		this(aBlock, aName, aMaterial, aSound, ItemTier.WOOD.getHarvestLevel(), ToolType.PICKAXE, 6.0f);
	}
	
	public BlockStairs(Block aBlock, String aName, Material aMaterial, SoundType aSound, int aHarvestLevel, ToolType aToolType, float aResistance)
	{
		super(() -> aBlock.getDefaultState(), Block.Properties.create(Material.ROCK).sound(aSound).harvestLevel(aHarvestLevel).harvestTool(aToolType).hardnessAndResistance(2.0f, aResistance));
		
		setRegistryName(aName + "_stair");
		NordicRegistry.QueueBlockForRegister(this);
	}

	@Override
	public ItemGroup Group()
	{
		return NordicWorldItemGroup.Instance;
	}

	@Override
	public void SetGroup(ItemGroup aGroup)
	{
		
	}
}
