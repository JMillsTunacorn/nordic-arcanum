package com.lordskittles.nordicarcanum.blocks;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicWorldItemGroup;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.ItemTier;
import net.minecraftforge.common.ToolType;

public class BlockPlank extends BlockModBase
{
	public BlockPlank(String aName)
	{
		super(aName + "_plank", Material.WOOD, MaterialColor.WOOD, 2.0f, 3.0f, SoundType.WOOD, ItemTier.WOOD.getHarvestLevel(), ToolType.AXE);

		SetGroup(NordicWorldItemGroup.Instance);
	}
}
