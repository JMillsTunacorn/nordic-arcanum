package com.lordskittles.nordicarcanum.blocks.trees;

import java.util.Random;

import com.lordskittles.nordicarcanum.blocks.Blocks;
import com.lordskittles.nordicarcanum.world.feature.NordicFeature;

import net.minecraft.block.trees.Tree;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.TreeFeatureConfig;
import net.minecraft.world.gen.foliageplacer.BlobFoliagePlacer;
import net.minecraftforge.common.IPlantable;

public class TreeJuniper extends Tree
{
	public static final TreeFeatureConfig Config = (new TreeFeatureConfig.Builder(
			new SimpleBlockStateProvider(Blocks.juniper_log.getDefaultState()),
			new SimpleBlockStateProvider(Blocks.juniper_leaves.getDefaultState()), new BlobFoliagePlacer(3, 0)))
					.baseHeight(14).heightRandA(5).foliageHeight(9).ignoreVines()
					.setSapling((IPlantable) Blocks.juniper_sapling).build();

	@Override
	protected ConfiguredFeature<TreeFeatureConfig, ?> getTreeFeature(Random aRandom, boolean aBoolean)
	{
		return NordicFeature.JuniperTree.withConfiguration(Config);
	}
}
