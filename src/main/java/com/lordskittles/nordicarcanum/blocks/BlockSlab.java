package com.lordskittles.nordicarcanum.blocks;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicWorldItemGroup;
import com.lordskittles.nordicarcanum.api.NordicRegistry;

import net.minecraft.block.Block;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemTier;
import net.minecraftforge.common.ToolType;

public class BlockSlab extends SlabBlock implements IItemGroupHolder
{
	public BlockSlab(String aName)
	{
		this(aName, Material.ROCK, SoundType.STONE);
	}
	
	public BlockSlab(String aName, Material aMaterial, SoundType aSound)
	{
		this(aName, aMaterial, aSound, ItemTier.WOOD.getHarvestLevel(), ToolType.PICKAXE, 6.0f);
	}
	
	public BlockSlab(String aName, Material aMaterial, SoundType aSound, int aHarvestLevel, ToolType aToolType, float aResistance)
	{
		super(Block.Properties.create(Material.ROCK).sound(aSound).harvestLevel(aHarvestLevel).harvestTool(aToolType).hardnessAndResistance(2.0f, aResistance));
		
		setRegistryName(aName + "_slab");
		NordicRegistry.QueueBlockForRegister(this);
	}

	@Override
	public ItemGroup Group()
	{
		return NordicWorldItemGroup.Instance;
	}

	@Override
	public void SetGroup(ItemGroup aGroup)
	{
		// TODO Auto-generated method stub
		
	}
}
