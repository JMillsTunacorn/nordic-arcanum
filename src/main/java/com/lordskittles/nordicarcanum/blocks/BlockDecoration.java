package com.lordskittles.nordicarcanum.blocks;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicWorldItemGroup;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;

public class BlockDecoration extends BlockModBase
{
	public BlockDecoration(String aName, Material aMaterial, MaterialColor aMaterialColor)
	{
		super(aName + "_deco", aMaterial, aMaterialColor, 1.5f, 6.0f, SoundType.STONE);

		SetGroup(NordicWorldItemGroup.Instance);
	}
	
	public BlockDecoration(String aName, float aHardness)
	{
		super(aName + "_deco", Material.ROCK, MaterialColor.STONE, aHardness, 6.0f, SoundType.STONE);

		SetGroup(NordicWorldItemGroup.Instance);
	}
	
	public BlockDecoration(String aName, int aLightLevel)
	{
		super(aName + "_deco", Material.GLASS, MaterialColor.GOLD, 0.5f, 3.0f, SoundType.GLASS, aLightLevel);

		SetGroup(NordicWorldItemGroup.Instance);
	}
	
	public BlockDecoration(String aName, Material aMaterial, MaterialColor aMaterialColor, int aHarvestLevel)
	{
		super(aName + "_deco", aMaterial, aMaterialColor, 0.5f, 3.0f, SoundType.GLASS, 0, aHarvestLevel);
		
		SetGroup(NordicWorldItemGroup.Instance);
	}
}
