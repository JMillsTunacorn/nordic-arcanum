package com.lordskittles.nordicarcanum.blocks;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicWorldItemGroup;
import com.lordskittles.nordicarcanum.api.NordicRegistry;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.LogBlock;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.ItemGroup;

public class BlockLog extends LogBlock implements IItemGroupHolder
{
	public BlockLog(String aName)
	{
		super(MaterialColor.WOOD, Block.Properties.from(Blocks.OAK_LOG));
		
		setRegistryName(aName + "_log");
		NordicRegistry.QueueBlockForRegister(this);
	}

	@Override
	public ItemGroup Group()
	{
		return NordicWorldItemGroup.Instance;
	}

	@Override
	public void SetGroup(ItemGroup aGroup)
	{
		
	}
}
