package com.lordskittles.nordicarcanum.blocks;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicWorldItemGroup;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.ItemTier;
import net.minecraftforge.common.ToolType;

public class BlockCompactResource extends BlockModBase
{
	public BlockCompactResource(String aResourceName)
	{
		super(aResourceName + "_compact", Material.IRON, MaterialColor.RED, 5.0F, 6.0F, SoundType.METAL, ItemTier.WOOD.getHarvestLevel(), ToolType.PICKAXE);
		
		SetGroup(NordicWorldItemGroup.Instance);
	}
}
