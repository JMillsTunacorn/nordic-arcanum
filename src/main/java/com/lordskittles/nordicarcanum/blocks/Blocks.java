package com.lordskittles.nordicarcanum.blocks;

import com.lordskittles.nordicarcanum.NordicArcanum;
import com.lordskittles.nordicarcanum.api.NordicRegistry;
import com.lordskittles.nordicarcanum.blocks.trees.TreeJuniper;
import com.lordskittles.nordicarcanum.blocks.trees.TreePine;
import com.lordskittles.nordicarcanum.blocks.trees.TreeYew;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.ItemTier;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(NordicArcanum.MODID)
@Mod.EventBusSubscriber(modid = NordicArcanum.MODID, bus = Bus.MOD)
public class Blocks
{
	public static final Block arcane_dust_compact = new BlockCompactResource("arcane_dust"); 
	public static final Block carnelian_crystal_compact = new BlockCompactResource("carnelian_crystal");
	public static final Block calcite_crystal_compact = new BlockCompactResource("calcite_crystal");
	public static final Block garnet_crystal_compact = new BlockCompactResource("garnet_crystal");
	public static final Block amethyst_crystal_compact = new BlockCompactResource("amethyst_crystal");
	public static final Block silver_metal_compact = new BlockCompactResource("silver_metal");
	public static final Block nickle_metal_compact = new BlockCompactResource("nickle_metal");
	public static final Block lead_metal_compact = new BlockCompactResource("lead_metal");
	public static final Block copper_metal_compact = new BlockCompactResource("copper_metal");
	public static final Block steel_metal_compact = new BlockCompactResource("steel_metal");
	public static final Block black_metal_compact = new BlockCompactResource("black_metal");
	
	public static final Block arcane_dust_ore = new BlockOre("arcane_dust", ItemTier.STONE.getHarvestLevel(), 4);
	public static final Block carnelian_ore = new BlockOre("carnelian", ItemTier.IRON.getHarvestLevel(), 0);
	public static final Block calcite_ore = new BlockOre("calcite", ItemTier.STONE.getHarvestLevel(), 0, 1.5F);
	public static final Block garnet_ore = new BlockOre("garnet", ItemTier.IRON.getHarvestLevel(), 0);
	public static final Block amethyst_ore = new BlockOre("amethyst", ItemTier.IRON.getHarvestLevel(), 0);
	public static final Block silver_ore = new BlockOre("silver", ItemTier.IRON.getHarvestLevel(), 0);
	public static final Block nickle_ore = new BlockOre("nickle", ItemTier.IRON.getHarvestLevel(), 0);
	public static final Block copper_ore = new BlockOre("copper", ItemTier.IRON.getHarvestLevel(), 0);
	public static final Block lead_ore = new BlockOre("lead", ItemTier.IRON.getHarvestLevel(), 0);
	
	public static final Block yew_plank = new BlockPlank("yew");
	public static final BlockLog yew_log = new BlockLog("yew");
	public static final Block yew_leaves = new BlockLeaves("yew");
	public static final Block yew_sapling = new BlockSapling(() -> new TreeYew(), "yew");
	
	public static final Block pine_plank = new BlockPlank("pine");
	public static final BlockLog pine_log = new BlockLog("pine");
	public static final Block pine_leaves = new BlockLeaves("pine");
	public static final Block pine_sapling = new BlockSapling(() -> new TreePine(), "pine");
	
	public static final Block juniper_plank = new BlockPlank("juniper");
	public static final BlockLog juniper_log = new BlockLog("juniper");
	public static final Block juniper_leaves = new BlockLeaves("juniper");
	public static final Block juniper_sapling = new BlockSapling(() -> new TreeJuniper(), "juniper");
	
	public static final Block refined_calcite_deco = new BlockDecoration("refined_calcite", Material.ROCK, MaterialColor.SNOW);
	public static final Block calcite_brick_deco = new BlockDecoration("calcite_brick", Material.ROCK, MaterialColor.SNOW);
	public static final Block chiseled_calcite_brick_deco = new BlockDecoration("chiseled_calcite_brick", Material.ROCK, MaterialColor.SNOW);

	public static final Block refined_calcite_slab = new BlockSlab("refined_calcite");
	public static final Block calcite_brick_slab = new BlockSlab("calcite_brick");
	public static final Block yew_slab = new BlockSlab("yew", Material.WOOD, SoundType.WOOD, ItemTier.WOOD.getHarvestLevel(), ToolType.AXE, 3f);
	public static final Block pine_slab = new BlockSlab("pine", Material.WOOD, SoundType.WOOD, ItemTier.WOOD.getHarvestLevel(), ToolType.AXE, 3f);
	public static final Block juniper_slab = new BlockSlab("juniper", Material.WOOD, SoundType.WOOD, ItemTier.WOOD.getHarvestLevel(), ToolType.AXE, 3f);
	
	public static final Block refined_calcite_stair = new BlockStairs(refined_calcite_deco, "refined_calcite");
	public static final Block calcite_brick_stair = new BlockStairs(calcite_brick_deco, "calcite_brick");
	public static final Block yew_stair = new BlockStairs(yew_plank, "yew", Material.WOOD, SoundType.WOOD, ItemTier.WOOD.getHarvestLevel(), ToolType.AXE, 3f);
	public static final Block pine_stair = new BlockStairs(pine_plank, "pine", Material.WOOD, SoundType.WOOD, ItemTier.WOOD.getHarvestLevel(), ToolType.AXE, 3f);
	public static final Block juniper_stair = new BlockStairs(juniper_plank, "juniper", Material.WOOD, SoundType.WOOD, ItemTier.WOOD.getHarvestLevel(), ToolType.AXE, 3f);
	
	public static final Block sigil_podium = new BlockSigilPodium();
	public static final Block staff_workbench = new BlockStaffWorkbench();
	public static final Block attunement_altar = new BlockAttunementAltar();
	public static final Block crafting_cloth = new BlockCraftingCloth();
	
	@SubscribeEvent
	public static void registerBlocks(final RegistryEvent.Register<Block> aEvent)
	{
		NordicRegistry.RegisterBlocks(aEvent);
	}
}
