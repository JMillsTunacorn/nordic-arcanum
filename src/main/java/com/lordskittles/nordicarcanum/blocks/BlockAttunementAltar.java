package com.lordskittles.nordicarcanum.blocks;

import javax.annotation.Nullable;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicItemGroup;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemTier;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;

public class BlockAttunementAltar extends BlockModBase
{
	protected static VoxelShape WHOLE = Block.makeCuboidShape(1D, 0D, 1D, 15D, 20D, 15D);
	
	public BlockAttunementAltar()
	{
		super("attunement_altar", Material.ROCK, MaterialColor.GRAY, 1.5f, 6.0f, SoundType.STONE, 4, ItemTier.STONE.getHarvestLevel(), ToolType.PICKAXE);
		
		SetGroup(NordicItemGroup.Instance);
	}	
	
	@Override
	public VoxelShape getShape(BlockState aState, IBlockReader aWorld, BlockPos aPos, ISelectionContext aContext)
	{
		return WHOLE;
	}
	
	@Override
	public boolean isNormalCube(BlockState aState, IBlockReader aWorld, BlockPos aPos)
	{
		return false;
	}
	
	@Override
	public ActionResultType onBlockActivated(BlockState aState, World aWorldIn, BlockPos aPos, PlayerEntity aPlayer, Hand aHandIn, BlockRayTraceResult aHit) 
	{
		return ActionResultType.SUCCESS;
	}
	
	@Nullable
	@Override
	public INamedContainerProvider getContainer(BlockState aState, World aWorldIn, BlockPos aPos) 
	{
		return null;
	}
}
