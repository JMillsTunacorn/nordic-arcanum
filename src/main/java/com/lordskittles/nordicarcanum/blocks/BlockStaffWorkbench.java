package com.lordskittles.nordicarcanum.blocks;

import javax.annotation.Nullable;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicItemGroup;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.HorizontalBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemTier;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;

public class BlockStaffWorkbench extends BlockModBase
{
	public static final DirectionProperty FACING = HorizontalBlock.HORIZONTAL_FACING;
	
	protected static VoxelShape X_PART_TABLE = Block.makeCuboidShape(-8D, 13D, 0D, 24D, 16D, 16D);
	protected static VoxelShape X_LEG_BACK_LEFT = Block.makeCuboidShape(-8D, 0D, 12D, -4D, 13D, 16D);
	protected static VoxelShape X_LEG_FRONT_LEFT = Block.makeCuboidShape(-8D, 0D, 0D, -4D, 13D, 4D);
	protected static VoxelShape X_LEG_FRONT_RIGHT = Block.makeCuboidShape(21D, 0D, 0D, 24D, 13D, 4D);
	protected static VoxelShape X_LEG_BACK_RIGHT = Block.makeCuboidShape(21D, 0D, 12D, 24D, 13D, 16D);
	protected static VoxelShape X_PART_TOP = Block.makeCuboidShape(-8D, 16D, 11D, 24D, 32D, 16D);
	protected static VoxelShape X_AXIS = VoxelShapes.or(X_PART_TABLE, X_LEG_BACK_LEFT, X_LEG_BACK_RIGHT, X_LEG_FRONT_RIGHT, X_LEG_FRONT_LEFT, X_PART_TOP);
	
	protected static VoxelShape Z_PART_TABLE = Block.makeCuboidShape(0D, 13D, -8D, 16D, 16D, 24D);
	protected static VoxelShape Z_LEG_BACK_LEFT = Block.makeCuboidShape(12D, 0D, -8D, 16D, 13D, -4D);
	protected static VoxelShape Z_LEG_FRONT_LEFT = Block.makeCuboidShape(0D, 0D, -8D, 4D, 13D, -4D);
	protected static VoxelShape Z_LEG_FRONT_RIGHT = Block.makeCuboidShape(0D, 0D, 21D, 4D, 13D, 24D);
	protected static VoxelShape Z_LEG_BACK_RIGHT = Block.makeCuboidShape(12D, 0D, 21D, 16D, 13D, 24D);
	protected static VoxelShape Z_PART_TOP = Block.makeCuboidShape(11D, 16D, -8D, 16D, 32D, 24D);
	protected static VoxelShape Z_AXIS = VoxelShapes.or(Z_PART_TABLE, Z_LEG_BACK_LEFT, Z_LEG_BACK_RIGHT, Z_LEG_FRONT_RIGHT, Z_LEG_FRONT_LEFT, Z_PART_TOP);
	
	public BlockStaffWorkbench()
	{
		super("staff_workbench", Material.WOOD, MaterialColor.WOOD, 1.5F, 6.0F, SoundType.WOOD, 4, ItemTier.WOOD.getHarvestLevel(), ToolType.AXE);
		this.setDefaultState(this.stateContainer.getBaseState().with(FACING, Direction.NORTH));
		
		SetGroup(NordicItemGroup.Instance);
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext aContext) 
	{
		return this.getDefaultState().with(FACING, aContext.getPlacementHorizontalFacing().rotateY().getOpposite());
	}
	
	@Override
	public boolean isNormalCube(BlockState aState, IBlockReader aWorld, BlockPos aPos)
	{
		return false;
	}
	
	@Override
	public ActionResultType onBlockActivated(BlockState aState, World aWorldIn, BlockPos aPos, PlayerEntity aPlayer, Hand aHandIn, BlockRayTraceResult aHit) 
	{
		return ActionResultType.SUCCESS;
	}
	
	@Nullable
	@Override
	public INamedContainerProvider getContainer(BlockState aState, World aWorldIn, BlockPos aPos) 
	{
		return null;
	}
	
	@Override
	public VoxelShape getShape(BlockState aState, IBlockReader aWorld, BlockPos aPos, ISelectionContext aContext)
	{
		Direction direction = aState.get(FACING);
		return direction.getAxis() == Direction.Axis.X ? X_AXIS : Z_AXIS;
	}
	
	@Override
	protected void fillStateContainer(StateContainer.Builder<Block, BlockState> aBuilder) 
	{
		aBuilder.add(FACING);
	}
	
	/**
	    * Returns the blockstate with the given rotation from the passed blockstate. If inapplicable, returns the passed
	    * blockstate.
	    * @deprecated call via {@link IBlockState#withRotation(Rotation)} whenever possible. Implementing/overriding is
	    * fine.
	    */
	@Override
	public BlockState rotate(BlockState aState, Rotation aRot) 
	{
		return aState.with(FACING, aRot.rotate(aState.get(FACING)));
	}
	
	/**
	    * Returns the blockstate with the given mirror of the passed blockstate. If inapplicable, returns the passed
	    * blockstate.
	    * @deprecated call via {@link IBlockState#withMirror(Mirror)} whenever possible. Implementing/overriding is fine.
	    */
	@Override
	public BlockState mirror(BlockState aState, Mirror aMirror)
	{
		return aState.rotate(aMirror.toRotation(aState.get(FACING)));
	}
}
