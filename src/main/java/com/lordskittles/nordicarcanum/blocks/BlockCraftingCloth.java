package com.lordskittles.nordicarcanum.blocks;

import javax.annotation.Nullable;

import com.lordskittles.nordicarcanum.NordicArcanum.NordicItemGroup;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockCraftingCloth extends BlockModBase
{
	protected static VoxelShape WHOLE = Block.makeCuboidShape(0D, 0D, 0D, 16D, 1D, 16D);
	
	public BlockCraftingCloth()
	{
		super("crafting_cloth", Material.WOOL, MaterialColor.WOOL, 0.1f, 0.1f, SoundType.CLOTH, 0);
		
		SetGroup(NordicItemGroup.Instance);
	}
	
	@Override
	public VoxelShape getShape(BlockState aState, IBlockReader aWorld, BlockPos aPos, ISelectionContext aContext)
	{
		return WHOLE;
	}
	
	@Override
	public boolean isNormalCube(BlockState aState, IBlockReader aWorld, BlockPos aPos)
	{
		return false;
	}
	
	@Override
	public ActionResultType onBlockActivated(BlockState aState, World aWorldIn, BlockPos aPos, PlayerEntity aPlayer, Hand aHandIn, BlockRayTraceResult aHit) 
	{
		return ActionResultType.SUCCESS;
	}
	
	@Nullable
	@Override
	public INamedContainerProvider getContainer(BlockState aState, World aWorldIn, BlockPos aPos) 
	{
		return null;
	}
}
