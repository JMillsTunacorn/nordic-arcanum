package com.lordskittles.nordicarcanum.blocks;

import com.lordskittles.nordicarcanum.api.NordicRegistry;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.common.ToolType;

public class BlockModBase extends Block implements IItemGroupHolder
{
	protected String mRegistryName = "";
	protected ItemGroup mGroup;
	
	public BlockModBase(String aRegistryName, Material aMaterial, MaterialColor aColor)
	{
		this(aRegistryName, aMaterial, aColor, 0, 15, SoundType.GROUND);
	}
	
	public BlockModBase(String aRegistryName, Material aMaterial, MaterialColor aColor, float aHardness, float aResistance)
	{
		this(aRegistryName, aMaterial, aColor, aHardness, aResistance, SoundType.GROUND);
	}
	
	public BlockModBase(String aRegistryName, Material aMaterial, MaterialColor aColor, float aHardness, float aResistance, SoundType aSoundType)
	{
		this(aRegistryName, aMaterial, aColor, aHardness, aResistance, aSoundType, 0);
	}
	
	public BlockModBase(String aRegistryName, Material aMaterial, MaterialColor aColor, float aHardness, float aResistance, SoundType aSoundType, int aLightLevel)
	{
		this(aRegistryName, aMaterial, aColor, aHardness, aResistance, aSoundType, aLightLevel, 0);
	}
	
	public BlockModBase(String aRegistryName, Material aMaterial, MaterialColor aColor, float aHardness, float aResistance, SoundType aSoundType, int aLightLevel, int aHarvestLevel)
	{
		this(aRegistryName, aMaterial, aColor, aHardness, aResistance, aSoundType, aLightLevel, aHarvestLevel, ToolType.PICKAXE);
	}
	
	public BlockModBase(String aRegistryName, Material aMaterial, MaterialColor aColor, float aHardness, float aResistance, SoundType aSoundType, int aLightLevel, int aHarvestLevel, ToolType aToolType)
	{
		super(Block.Properties.create(aMaterial, aColor).hardnessAndResistance(aHardness, aResistance).sound(aSoundType).lightValue(aLightLevel).harvestLevel(aHarvestLevel).harvestTool(aToolType));
		
		mRegistryName = aRegistryName;
		setRegistryName(mRegistryName);
		
		NordicRegistry.QueueBlockForRegister(this);
	}
	public BlockModBase(String aRegistryName, Material aMaterial, MaterialColor aColor, float aHardness, float aResistance, SoundType aSoundType, int aHarvestLevel, ToolType aToolType)
	{
		super(Block.Properties.create(aMaterial, aColor).hardnessAndResistance(aHardness, aResistance).harvestLevel(aHarvestLevel).harvestTool(aToolType).sound(aSoundType));
		
		mRegistryName = aRegistryName;
		setRegistryName(mRegistryName);
		
		NordicRegistry.QueueBlockForRegister(this);
	}
	
	public BlockModBase(String aRegistryName, Material aMaterial, MaterialColor aColor, float aHardness, float aResistance, int aHarvestLevel, ToolType aToolType)
	{
		super(Block.Properties.create(aMaterial, aColor).hardnessAndResistance(aHardness, aResistance).harvestLevel(aHarvestLevel).harvestTool(aToolType));
		
		mRegistryName = aRegistryName;
		setRegistryName(mRegistryName);
		
		NordicRegistry.QueueBlockForRegister(this);
	}

	public String GetNordicRegistryName()
	{
		return mRegistryName;
	}
	
	public ItemGroup Group()
	{
		return mGroup;
	}
	
	public void SetGroup(ItemGroup aGroup)
	{
		mGroup = aGroup;
	}
}
