package com.lordskittles.nordicarcanum.world;

import com.lordskittles.nordicarcanum.blocks.Blocks;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.placement.ConfiguredPlacement;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.registries.ForgeRegistries;

public class OreGeneration
{
	@SuppressWarnings("rawtypes")
	public static void GenerateOre()
	{
		for (Biome biome : ForgeRegistries.BIOMES)
		{
			ConfiguredPlacement customConfig = Placement.COUNT_RANGE.configure(new CountRangeConfig(20, 5, 5, 64));
			biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, Blocks.arcane_dust_ore.getDefaultState(), 10)).withPlacement(customConfig));
			
			customConfig = Placement.COUNT_RANGE.configure(new CountRangeConfig(1, 10, 5, 25));
			biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, Blocks.carnelian_ore.getDefaultState(), 5)).withPlacement(customConfig));
		
			customConfig = Placement.COUNT_RANGE.configure(new CountRangeConfig(1, 10, 5, 25));
			biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, Blocks.calcite_ore.getDefaultState(), 50)).withPlacement(customConfig));

			customConfig = Placement.COUNT_RANGE.configure(new CountRangeConfig(1, 10, 5, 25));
			biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, Blocks.garnet_ore.getDefaultState(), 5)).withPlacement(customConfig));
		}
	}
}
