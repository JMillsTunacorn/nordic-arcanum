package com.lordskittles.nordicarcanum.world.feature;

import java.util.Random;
import java.util.Set;
import java.util.function.Function;

import com.lordskittles.nordicarcanum.api.NordicRegistry;
import com.lordskittles.nordicarcanum.utilities.BlockUtilities;
import com.mojang.datafixers.Dynamic;

import net.minecraft.block.BlockState;
import net.minecraft.state.IProperty;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.IWorld;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.IWorldGenerationReader;
import net.minecraft.world.gen.feature.AbstractTreeFeature;
import net.minecraft.world.gen.feature.TreeFeatureConfig;
import net.minecraft.world.gen.placement.AtSurfaceWithExtraConfig;
import net.minecraft.world.gen.placement.Placement;

@SuppressWarnings("rawtypes")
public abstract class FeatureBaseTree extends AbstractTreeFeature<TreeFeatureConfig>
{
	protected IProperty mLogAxisProperty;
	
	protected BlockState mLog;
	protected TreeFeatureConfig mConfig;
	protected Random mRandom;
	protected IWorld mWorld;
	protected MutableBoundingBox mBounds;
	
	private Biome[] mBiomes;
	
	public FeatureBaseTree(String aName, BlockState aLog, Function<Dynamic<?>, ? extends TreeFeatureConfig> aConfig)
	{
		super(aConfig);
		
		mLog = aLog;
		mLogAxisProperty = BlockUtilities.getAxisProperty(mLog); 
		mBiomes = new Biome[] { Biomes.FOREST, Biomes.FLOWER_FOREST, Biomes.PLAINS, Biomes.BIRCH_FOREST_HILLS, Biomes.WOODED_HILLS };
		
		setRegistryName(aName + "_tree");
		NordicRegistry.QueueFeatureForRegister(this);
	}
	
	public void AddTreeToBiomes(TreeFeatureConfig aConfig)
	{
		for (Biome biome : mBiomes)
		{
			biome.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, this.withConfiguration(aConfig).withPlacement(Placement.COUNT_EXTRA_HEIGHTMAP.configure(new AtSurfaceWithExtraConfig(10, 100, 10))));
		}
	}

	@Override
	protected boolean func_225557_a_(IWorldGenerationReader aWorld, Random aRand, BlockPos aPos, Set<BlockPos> aStartPosSet, Set<BlockPos> aEndPosSet, MutableBoundingBox aBounds, TreeFeatureConfig aConfig)
	{
		mRandom = aRand;
		mConfig = aConfig;
		mWorld = (IWorld)aWorld;
		mBounds = aBounds;
		
		return place(aPos, aStartPosSet, aEndPosSet);
	}
	
	protected void PlaceLeafBox(BlockPos aStartPos, BlockPos aEndPos, Set<BlockPos> aChangedBlocks) 
	{
		for (int x = 0; x < aEndPos.getX(); x++)
		{
			for (int y = 0; y < aEndPos.getY(); y++)
			{
				for (int z = 0; z < aEndPos.getZ(); z++)
				{
					BlockPos leafPos = new BlockPos(aStartPos.getX() + x, aStartPos.getY() + y, aStartPos.getZ() + z);
					PlaceLeaf(leafPos, aChangedBlocks);
				}
			}
		}
	}
	
	protected void PlaceLeaf(BlockPos aPos, Set<BlockPos> aChangedBlocks)
	{		
		func_227219_b_(mWorld, mRandom, aPos, aChangedBlocks, mBounds, mConfig);
	}
	
	protected boolean PlaceLog(BlockPos aPos, Set<BlockPos> aChangedBlocks)
	{
		return PlaceLog(aPos, (Direction.Axis)null, aChangedBlocks);
	}

	@SuppressWarnings("unchecked")
	protected boolean PlaceLog(BlockPos aPos, Direction.Axis aAxis, Set<BlockPos> aChangedBlocks)
	{
		BlockState directedLog = (aAxis != null && this.mLogAxisProperty != null) ? (BlockState)mLog.with(this.mLogAxisProperty, (Comparable)aAxis) : mLog;
		return PlaceBlock(aPos, directedLog, aChangedBlocks);
	}
	
	private boolean PlaceBlock(BlockPos aPos, BlockState aState, Set<BlockPos> aChangedBlocks)
	{
		if (isAirOrLeaves(mWorld, aPos))
		{
			this.func_227217_a_(mWorld, aPos, aState, mBounds);
			aChangedBlocks.add(aPos.toImmutable());
			
			return true;
		}
		
		return false;
	}
	
	protected boolean place(BlockPos aPos, Set<BlockPos> aLogs, Set<BlockPos> aLeaves)
	{
		if (!isSoil(mWorld, aPos.down(), mConfig.getSapling())) 
		{
			return false;
		}
		else
		{
			BlockPos pos = aPos.down();
			
			this.setDirtAt(mWorld, pos, aPos);

			PlaceTrunk(aPos, aLogs);
			PlaceLeaves(aPos, aLeaves);
			
			return true;
		}
	}
	
	protected abstract void PlaceTrunk(BlockPos aPos, Set<BlockPos> aLogSet);
	protected abstract void PlaceLeaves(BlockPos aPos, Set<BlockPos> aLeafSet);
}
