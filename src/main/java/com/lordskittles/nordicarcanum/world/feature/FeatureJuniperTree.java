package com.lordskittles.nordicarcanum.world.feature;

import java.util.Set;
import java.util.function.Function;

import com.lordskittles.nordicarcanum.blocks.Blocks;
import com.mojang.datafixers.Dynamic;

import net.minecraft.util.Direction.Axis;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.feature.TreeFeatureConfig;

public class FeatureJuniperTree extends FeatureBaseTree
{
	public FeatureJuniperTree(Function<Dynamic<?>, ? extends TreeFeatureConfig> aConfig)
	{
		super("juniper", Blocks.juniper_log.getDefaultState(), aConfig);
	}

	@Override
	protected void PlaceTrunk(BlockPos aPos, Set<BlockPos> aLogs)
	{
		BlockPos pos = new BlockPos(aPos.getX(), aPos.getY(), aPos.getZ());
		PlaceLog(pos, aLogs);
		
		pos = new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ());
		PlaceLog(pos, aLogs);

		pos = new BlockPos(pos.getX() + 1, pos.getY(), pos.getZ());
		PlaceLog(pos, aLogs);

		pos = new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ());
		PlaceLog(pos, aLogs);

		pos = new BlockPos(pos.getX() + 1, pos.getY(), pos.getZ());
		PlaceLog(pos, aLogs);

		pos = new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ());
		PlaceLog(pos, aLogs);

		pos = new BlockPos(pos.getX(), pos.getY(), pos.getZ() + 1);
		PlaceLog(pos, Axis.Z, aLogs);

		pos = new BlockPos(pos.getX(), pos.getY(), pos.getZ() + 1);
		PlaceLog(pos, aLogs);

		pos = new BlockPos(pos.getX() - 1, pos.getY(), pos.getZ());
		for(int y = 0; y < 4; y++)
		{
			BlockPos logPos = new BlockPos(pos.getX(), pos.getY() + y, pos.getZ());
			PlaceLog(logPos, aLogs);
		}

		pos = new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() - 2);
		for (int z = 0; z < 5; z++)
		{
			BlockPos logPos = new BlockPos(pos.getX(), pos.getY(), pos.getZ() + z);
			PlaceLog(logPos, Axis.Z, aLogs);
		}
		
		pos = new BlockPos(pos.getX() - 2, pos.getY(), pos.getZ() + 2);
		for (int x = 0; x < 5; x++)
		{
			BlockPos logPos = new BlockPos(pos.getX() + x, pos.getY(), pos.getZ());
			PlaceLog(logPos, Axis.X, aLogs);
		}
	}

	@Override
	protected void PlaceLeaves(BlockPos aPos, Set<BlockPos> aLeaves)
	{
		// Bottom Leaf Layer
		BlockPos pos = new BlockPos(aPos.getX(), aPos.getY() + 4, aPos.getZ());
		PlaceLeafBox(pos, new BlockPos(3, 1, 5), aLeaves);
		
		pos = new BlockPos(aPos.getX() - 1, pos.getY(), aPos.getZ() + 1);
		PlaceLeafBox(pos, new BlockPos(5, 1, 3), aLeaves);
		
		// Second Leaf Layer
		pos = new BlockPos(aPos.getX() - 1, pos.getY() + 1, aPos.getZ());
		PlaceLeafBox(pos, new BlockPos(5, 1, 5), aLeaves);

		pos = new BlockPos(aPos.getX() - 2, pos.getY(), aPos.getZ() + 1);
		PlaceLeafBox(pos, new BlockPos(7, 1, 3), aLeaves);

		pos = new BlockPos(aPos.getX(), pos.getY(), aPos.getZ() - 1);
		PlaceLeafBox(pos, new BlockPos(3, 1, 7), aLeaves);

		// Third Leaf Layer
		pos = new BlockPos(aPos.getX(), pos.getY() + 1, aPos.getZ() + 1);
		PlaceLeafBox(pos, new BlockPos(3, 1, 3), aLeaves);
		
		PlaceLeaf(new BlockPos(aPos.getX() + 1, pos.getY(), aPos.getZ()), aLeaves);
		PlaceLeaf(new BlockPos(aPos.getX() + 3, pos.getY(), aPos.getZ() + 2), aLeaves);
		PlaceLeaf(new BlockPos(aPos.getX() - 1, pos.getY(), aPos.getZ() + 2), aLeaves);
		PlaceLeaf(new BlockPos(aPos.getX() + 1, pos.getY(), aPos.getZ() + 4), aLeaves);
		
		// Top Leaf Layer
		pos = new BlockPos(aPos.getX() + 1, pos.getY() + 1, aPos.getZ() + 1);
		PlaceLeafBox(pos, new BlockPos(1, 1, 3), aLeaves);

		pos = new BlockPos(aPos.getX(), pos.getY(), aPos.getZ() + 2);
		PlaceLeafBox(pos, new BlockPos(3, 1, 1), aLeaves);
	}
}
