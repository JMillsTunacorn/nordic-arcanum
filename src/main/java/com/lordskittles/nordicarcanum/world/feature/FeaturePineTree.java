package com.lordskittles.nordicarcanum.world.feature;

import java.util.Set;
import java.util.function.Function;

import com.lordskittles.nordicarcanum.blocks.Blocks;
import com.mojang.datafixers.Dynamic;

import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.feature.TreeFeatureConfig;

public class FeaturePineTree extends FeatureBaseTree
{
	public FeaturePineTree(Function<Dynamic<?>, ? extends TreeFeatureConfig> aConfig)
	{
		super("pine", Blocks.pine_log.getDefaultState(), aConfig);
	}

	@Override
	protected void PlaceTrunk(BlockPos aPos, Set<BlockPos> aLogSet)
	{
		BlockPos pos = aPos;
		
		for	(int iter = 0; iter < 9; iter++)
		{
			pos = new BlockPos(aPos.getX(), aPos.getY() + iter, aPos.getZ());
			PlaceLog(pos, aLogSet);
		}

		PlaceLog(new BlockPos(aPos.getX() + 1, aPos.getY() + 4, aPos.getZ()), Direction.Axis.X, aLogSet);
		PlaceLog(new BlockPos(aPos.getX() - 1, aPos.getY() + 4, aPos.getZ()), Direction.Axis.X, aLogSet);
		PlaceLog(new BlockPos(aPos.getX(), aPos.getY() + 4, aPos.getZ() + 1), Direction.Axis.Z, aLogSet);
		PlaceLog(new BlockPos(aPos.getX(), aPos.getY() + 4, aPos.getZ() - 1), Direction.Axis.Z, aLogSet);
	}

	@Override
	protected void PlaceLeaves(BlockPos aPos, Set<BlockPos> aLeafSet)
	{
		// Main Leaf Part
		BlockPos pos = new BlockPos(aPos.getX() - 1, aPos.getY() + 4, aPos.getZ() - 1);
		PlaceLeafBox(pos, new BlockPos(3, 4, 3), aLeafSet);
		
		// Side Tall Leaves
		pos = new BlockPos(aPos.getX() - 2, pos.getY(), aPos.getZ());
		PlaceLeafBox(pos, new BlockPos(1, 2, 1), aLeafSet);

		pos = new BlockPos(aPos.getX() + 2, pos.getY(), aPos.getZ());
		PlaceLeafBox(pos, new BlockPos(1, 2, 1), aLeafSet);

		pos = new BlockPos(aPos.getX(), pos.getY(), aPos.getZ() - 2);
		PlaceLeafBox(pos, new BlockPos(1, 2, 1), aLeafSet);

		pos = new BlockPos(aPos.getX(), pos.getY(), aPos.getZ() + 2);
		PlaceLeafBox(pos, new BlockPos(1, 2, 1), aLeafSet);

		// Top Tall Leaves
		pos = new BlockPos(aPos.getX() + 1, aPos.getY() + 8, aPos.getZ());
		PlaceLeafBox(pos, new BlockPos(1, 2, 1), aLeafSet);

		pos = new BlockPos(aPos.getX() - 1, aPos.getY() + 8, aPos.getZ());
		PlaceLeafBox(pos, new BlockPos(1, 2, 1), aLeafSet);

		pos = new BlockPos(aPos.getX(), aPos.getY() + 8, aPos.getZ() + 1);
		PlaceLeafBox(pos, new BlockPos(1, 2, 1), aLeafSet);
		
		pos = new BlockPos(aPos.getX(), aPos.getY() + 8, aPos.getZ() - 1);
		PlaceLeafBox(pos, new BlockPos(1, 2, 1), aLeafSet);

		// Middle Tall Leaves
		pos = new BlockPos(aPos.getX(), aPos.getY() + 9, aPos.getZ());
		PlaceLeafBox(pos, new BlockPos(1, 3, 1), aLeafSet);
		
		// Bottom Leaves
		PlaceLeaf(new BlockPos(aPos.getX() + 1, aPos.getY() + 3, aPos.getZ()), aLeafSet);
		PlaceLeaf(new BlockPos(aPos.getX() - 1, aPos.getY() + 3, aPos.getZ()), aLeafSet);
		PlaceLeaf(new BlockPos(aPos.getX(), aPos.getY() + 3, aPos.getZ() + 1), aLeafSet);
		PlaceLeaf(new BlockPos(aPos.getX(), aPos.getY() + 3, aPos.getZ() - 1), aLeafSet);
	}
}
