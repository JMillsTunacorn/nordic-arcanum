package com.lordskittles.nordicarcanum.world.feature;

import java.util.Set;
import java.util.function.Function;

import com.lordskittles.nordicarcanum.blocks.Blocks;
import com.mojang.datafixers.Dynamic;

import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.feature.TreeFeatureConfig;

public class FeatureYewTree extends FeatureBaseTree
{
	public FeatureYewTree(Function<Dynamic<?>, ? extends TreeFeatureConfig> aConfig)
	{
		super("yew", Blocks.yew_log.getDefaultState(), aConfig);
	}

	@Override
	protected void PlaceTrunk(BlockPos aPos, Set<BlockPos> aLogSet)
	{
		BlockPos pos = aPos;
		
		for	(int iter = 0; iter < 7; iter++)
		{
			pos = new BlockPos(aPos.getX(), aPos.getY() + iter, aPos.getZ());
			PlaceLog(pos, aLogSet);
		}
		
		for (int x = -2; x <= 2; x++)
		{			
			pos = new BlockPos(aPos.getX() + x, aPos.getY() + 4, aPos.getZ());
			PlaceLog(pos, Direction.Axis.X, aLogSet);
		}
		
		for (int z = -2; z <= 2; z++)
		{			
			pos = new BlockPos(aPos.getX(), aPos.getY() + 4, aPos.getZ() + z);
			PlaceLog(pos, Direction.Axis.Z, aLogSet);
		}

		PlaceLog(new BlockPos(aPos.getX() + 1, aPos.getY() + 6, aPos.getZ()), Direction.Axis.X, aLogSet);
		PlaceLog(new BlockPos(aPos.getX() - 1, aPos.getY() + 6, aPos.getZ()), Direction.Axis.X, aLogSet);
		PlaceLog(new BlockPos(aPos.getX(), aPos.getY() + 6, aPos.getZ() + 1), Direction.Axis.Z, aLogSet);
		PlaceLog(new BlockPos(aPos.getX(), aPos.getY() + 6, aPos.getZ() - 1), Direction.Axis.Z, aLogSet);
	}

	@Override
	protected void PlaceLeaves(BlockPos aPos, Set<BlockPos> aLeafSet)
	{
		// Main Leaf Box
		BlockPos pos = new BlockPos(aPos.getX() - 2, aPos.getY() + 3, aPos.getZ() - 2);
		PlaceLeafBox(pos, new BlockPos(5, 2, 5), aLeafSet);
		
		// Secondary Leaf Box
		pos = new BlockPos(aPos.getX() - 1, aPos.getY() + 5, aPos.getZ() - 2);
		PlaceLeafBox(pos, new BlockPos(3, 2, 5), aLeafSet);
		
		pos = new BlockPos(aPos.getX() - 2, aPos.getY() + 5, aPos.getZ() - 1);
		PlaceLeafBox(pos, new BlockPos(5, 2, 3), aLeafSet);
		
		// Top Leaf Box
		pos = new BlockPos(aPos.getX() - 1, aPos.getY() + 7, aPos.getZ() - 1);
		PlaceLeafBox(pos, new BlockPos(3, 1, 3), aLeafSet);
		
		// Side Leaf Boxes
		pos = new BlockPos(aPos.getX() + 3, aPos.getY() + 3, aPos.getZ() - 1);
		PlaceLeafBox(pos, new BlockPos(1, 2, 3), aLeafSet);
		
		pos = new BlockPos(aPos.getX() - 3, aPos.getY() + 3, aPos.getZ() - 1);
		PlaceLeafBox(pos, new BlockPos(1, 2, 3), aLeafSet);
		
		pos = new BlockPos(aPos.getX() - 1, aPos.getY() + 3, aPos.getZ() - 3);
		PlaceLeafBox(pos, new BlockPos(3, 2, 1), aLeafSet);

		pos = new BlockPos(aPos.getX() - 1, aPos.getY() + 3, aPos.getZ() + 3);
		PlaceLeafBox(pos, new BlockPos(3, 2, 1), aLeafSet);

		// Side Leaf Extensions
		PlaceLeaf(new BlockPos(aPos.getX() - 3, aPos.getY() + 5, aPos.getZ()), aLeafSet);
		PlaceLeaf(new BlockPos(aPos.getX(), aPos.getY() + 5, aPos.getZ() + 3), aLeafSet);
		PlaceLeaf(new BlockPos(aPos.getX() + 3, aPos.getY() + 5, aPos.getZ()), aLeafSet);
		PlaceLeaf(new BlockPos(aPos.getX(), aPos.getY() + 5, aPos.getZ() - 3), aLeafSet);
	}
}
