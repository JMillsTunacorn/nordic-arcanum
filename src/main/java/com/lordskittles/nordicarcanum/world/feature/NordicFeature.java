package com.lordskittles.nordicarcanum.world.feature;

import com.lordskittles.nordicarcanum.api.NordicRegistry;
import com.lordskittles.nordicarcanum.blocks.trees.TreeJuniper;
import com.lordskittles.nordicarcanum.blocks.trees.TreePine;
import com.lordskittles.nordicarcanum.blocks.trees.TreeYew;

import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.TreeFeatureConfig;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public final class NordicFeature
{
	public static final FeatureBaseTree YewTree = new FeatureYewTree(TreeFeatureConfig::func_227338_a_);
	public static final FeatureBaseTree PineTree = new FeaturePineTree(TreeFeatureConfig::func_227338_a_);
	public static final FeatureBaseTree JuniperTree = new FeatureJuniperTree(TreeFeatureConfig::func_227338_a_);
	
	@SubscribeEvent
	public static void registerFeatures(final RegistryEvent.Register<Feature<?>> aEvent)
	{
		NordicRegistry.RegisterFeatures(aEvent);
		
		YewTree.AddTreeToBiomes(TreeYew.Config);
		PineTree.AddTreeToBiomes(TreePine.Config);
		JuniperTree.AddTreeToBiomes(TreeJuniper.Config);
	}
}
