package com.lordskittles.nordicarcanum;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lordskittles.nordicarcanum.blocks.Blocks;
import com.lordskittles.nordicarcanum.items.Items;
import com.lordskittles.nordicarcanum.world.OreGeneration;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod("nordicarcanum")
@Mod.EventBusSubscriber(modid = NordicArcanum.MODID, bus = Bus.MOD)
public class NordicArcanum
{
    public static final Logger LOG = LogManager.getLogger();
    public static final String MODID = "nordicarcanum";
    public static NordicArcanum Instance;
    
    public NordicArcanum() 
    {
    	Instance = this;
    	
    	final IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();
        modEventBus.addListener(this::setup);
        modEventBus.addListener(this::doClientStuff);

        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event)
    {
    	
    }

    private void doClientStuff(final FMLClientSetupEvent event) 
    {
    	
    }
    
    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) 
    {
    	
    }
    
    @SubscribeEvent
    public static void loadCompleteEvent(FMLLoadCompleteEvent event)
    {
    	OreGeneration.GenerateOre();
    }
    
    public static class NordicWorldItemGroup extends ItemGroup
    {
    	public static final NordicWorldItemGroup Instance = new NordicWorldItemGroup(ItemGroup.GROUPS.length, "nordicworldtab");
    	
    	private NordicWorldItemGroup(int aIndex, String aLabel)
    	{
    		super(aIndex, aLabel);
    	}
    	
    	@Override
    	public ItemStack createIcon()
    	{
    		return new ItemStack(Blocks.arcane_dust_ore);
    	}
    }
    
    public static class NordicItemGroup extends ItemGroup
    {
    	public static final NordicItemGroup Instance = new NordicItemGroup(ItemGroup.GROUPS.length, "nordictab");
    	
    	private NordicItemGroup(int aIndex, String aLabel)
    	{
    		super(aIndex, aLabel);
    	}
    	
    	@Override
    	public ItemStack createIcon()
    	{
    		return new ItemStack(Items.arcane_dust);
    	}
    }
}
