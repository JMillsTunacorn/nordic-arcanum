package com.lordskittles.nordicarcanum.utilities;

import com.lordskittles.nordicarcanum.NordicArcanum;
import com.lordskittles.nordicarcanum.blocks.Blocks;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(modid = NordicArcanum.MODID, bus = Bus.MOD, value = Dist.CLIENT)
public class ClientEventBusSubscriber
{
	@SubscribeEvent
	public static void clientSetup(FMLClientSetupEvent aEvent)
	{
		RenderTypeLookup.setRenderLayer(Blocks.yew_sapling, RenderType.getCutout());
		RenderTypeLookup.setRenderLayer(Blocks.pine_sapling, RenderType.getCutout());
		RenderTypeLookup.setRenderLayer(Blocks.juniper_sapling, RenderType.getCutout());
	}
}
