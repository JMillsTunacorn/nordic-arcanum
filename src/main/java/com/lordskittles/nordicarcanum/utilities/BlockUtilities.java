package com.lordskittles.nordicarcanum.utilities;

import java.util.Collection;

import net.minecraft.block.BlockState;
import net.minecraft.state.IProperty;
import net.minecraft.util.Direction;

public class BlockUtilities
{
	@SuppressWarnings("rawtypes")
	public static IProperty getAxisProperty(BlockState aLog) 
	{
	    for (IProperty property : aLog.getProperties()) 
	    {
			Collection allowedValues = property.getAllowedValues();
	    	if (allowedValues.contains(Direction.Axis.X) && allowedValues.contains(Direction.Axis.Y) && allowedValues.contains(Direction.Axis.Z)) 
	    	{
	    		return property; 
	    	}
	    } 
	    return null;
	}
	
	@SuppressWarnings("rawtypes")
	public static IProperty getPersistantProperty(BlockState aLeaf)
	{
		for	(IProperty property : aLeaf.getProperties())
		{
			Collection allowedValues = property.getAllowedValues();
			if (allowedValues.contains(Boolean.valueOf(true)) && allowedValues.contains(Boolean.valueOf(false)))
			{
				return property;
			}
		}
		
		return null;
	}
}
